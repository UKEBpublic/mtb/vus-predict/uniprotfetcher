# Stage 1: build maven project
FROM maven:3.8.1-jdk-8 AS build

WORKDIR /work/
COPY . /work/
RUN cd /work
RUN mvn install
RUN mvn package

# Stage 2: build image
FROM openjdk:8-jre-slim

WORKDIR /work/

COPY --from=build /work/target /work/target/
COPY src /work/src

RUN chmod 775 /work
RUN chmod 775 /work/target
RUN chmod +x /work/target

EXPOSE 9020
CMD ["java", "-jar", "/work/target/UniProtFetcher-1.0.0-SNAPSHOT-runner.jar"]