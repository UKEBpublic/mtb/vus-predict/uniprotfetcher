####
# This Dockerfile is used in order to build a container that runs the Quarkus application in native (no JVM) mode
#
# Before building the docker image run:
#
# mvn package -Pnative -Dnative-image.docker-build=true
#
# Then, build the image with:
#
# docker build -f src/main/docker/Dockerfile.native -t quarkus/UniProtFetcher .
#
# Then run the container using:
#
# docker run -i --rm -p 8080:8080 quarkus/UniProtFetcher
#
###

#FROM quay.io/quarkus/ubi-quarkus-native-image:19.3.1-java8 as nativebuilder
#FROM quay.io/quarkus/ubi-quarkus-native-image:21.1.0-java11 as nativebuilder

FROM quay.io/quarkus/centos-quarkus-maven:20.3.1-java11 AS build

# RUN mkdir -p /tmp/ssl-libs/lib \
#  && cp /opt/graalvm/jre/lib/security/cacerts /tmp/ssl-libs \
#  && cp /opt/graalvm/jre/lib/amd64/libsunec.so /tmp/ssl-libs/lib/

COPY pom.xml /usr/src/app/
RUN mvn -f /usr/src/app/pom.xml -B de.qaware.maven:go-offline-maven-plugin:1.2.5:resolve-dependencies
COPY src /usr/src/app/src
USER root
RUN chown -R quarkus /usr/src/app
USER quarkus
RUN mvn -f /usr/src/app/pom.xml -DskipTests -Pnative clean package

## Stage 2 : create the docker final image
FROM registry.access.redhat.com/ubi8/ubi-minimal
WORKDIR /work/
COPY --from=build /usr/src/app/target/*-runner /work/application
COPY src /work/src

# set up permissions for user `1001`
RUN chmod 775 /work /work/application \
  && chown -R 1001 /work \
  && chmod -R "g+rwX" /work \
  && chown -R 1001:root /work

# COPY pom.xml /project/
# COPY mvnw /project/mvnw
# COPY ".mvn" /project/.mvn

# USER quarkus
# WORKDIR /project
# RUN ./mvnw -B org.apache.maven.plugins:maven-dependency-plugin:3.1.2:go-offline
# COPY src /project/src

# RUN  ./mvnw package -Pnative -DskipTests


# FROM registry.access.redhat.com/ubi8/ubi-minimal
# WORKDIR /work/
# COPY --from=nativebuilder /project/target/*-runner /work/application

# RUN chmod 775 /work /work/application \
#   && chown -R 1001 /work \
#   && chmod -R "g+rwX" /work \
#   && chown -R 1001:root /work

EXPOSE 9020
CMD ["./application", "-Dquarkus.http.host=0.0.0.0", "-H:EnableURLProtocols=https", "-Djava.library.path=/work/lib", "-Djavax.net.ssl.trustStore=/work/cacerts"]
