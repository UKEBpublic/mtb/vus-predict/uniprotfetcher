package eu.UMG.bioinformatik.MTB.UniProtFetcher;

import java.util.Comparator;

class UniprotQualityComparator implements Comparator<UniProtQuality> {

    @Override
    public int compare(UniProtQuality o1, UniProtQuality o2) {
        if (o1.isObsolete() && !o2.isObsolete()) {
            return 1;
        }
        if (!o1.isObsolete() && o2.isObsolete()) {
            return -1;
        }
        if (o1.isReviewed() && !o2.isReviewed()) {
            return -1;
        }
        if (!o1.isReviewed() && o2.isReviewed()) {
            return 1;
        }
        return (((Integer) o1.getLevel()).compareTo((Integer) o2.getLevel()));
    }

}