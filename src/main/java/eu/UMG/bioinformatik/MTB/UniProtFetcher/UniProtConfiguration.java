package eu.UMG.bioinformatik.MTB.UniProtFetcher;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author juergen.doenitz@bioinf.med.uni-goettingen.de
 */
public class UniProtConfiguration {

    private String dataDir = "data/";

    @JsonProperty
    public void setDataDir(String dataDir) {
        this.dataDir = dataDir;
    }

    @JsonProperty
    public String getDataDir() {
        return dataDir;
    }
}
