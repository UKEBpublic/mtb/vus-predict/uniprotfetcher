package eu.UMG.bioinformatik.MTB.UniProtFetcher;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import java.util.*;

import java.io.*;
import javax.inject.Inject;
import javax.xml.xpath.XPathExpressionException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ws.rs.PathParam;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


/**
 * Ressource to provide input for UniProtConnector
 * @author vas
 */
@javax.ws.rs.Path("/toUniProt")
public class FetcherRessource {

    private final Helper helper = new Helper();
    private final UniProtConfiguration configuration = new UniProtConfiguration();
    private static final int SVG_LINE_HEIGHT = 25;
    
    // get cache Map from Class Cache
    @Inject
    Cache cache;

    /**
     * for tests only (FetcherRessourceTest.java)
     * @return "hello"
     */
    @javax.ws.rs.Path("/hello")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "hello";
    }
    
    /**
     * 
     * @param genesymbol: desired input of gene symbols (e.g. ?genesymbol=BRCA1,P53)
     * @return HashMap containing all gene information being extracted from UniProt
     * example for gene symbol P53:
     * {P53     {gene_info  {UniProtID:         P04637
     *                      }
     *          }
     *          {PDB_info   {1A1U   :   A/C=324-358
     *                       1AIE   :   A=326-356
     *                        ... 
     *                      }
     *          }
     * }
     * @throws IOException
     * @throws XPathExpressionException 
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @javax.ws.rs.Path("/ProtInfoByGeneSymbol")
    public Map<String, Map<String, Map<String, String>>> uniProtFetcherByName(@QueryParam("genesymbol") String genesymbol) throws IOException, XPathExpressionException {
        
        // check whether gene symbol or UniProtID is given
        if (genesymbol != null) {
            
            // fill gene_map
            return mapFromGeneSymbol(genesymbol);
 
        } else {
            // no valid input is given
            throw new IllegalArgumentException("Usage: localhost:8080/toUniProt/ProtInfo?genesymbol=BRCA1,... OR localhost:8080/toUniProt/ProtInfo?id=P38398,...");
        }

    }

    /**
     * 
     * @param id: desired input of UniProtIDs (e.g. ?id=P38398,P04637)
     * @return HashMap containing all gene information being extracted from UniProt
     * example for gene symbol P53:
     * {P53     {gene_info  {UniProtID:         P04637
     *                      }
     *          }
     *          {PDB_info   {1A1U   :   A/C=324-358
     *                       1AIE   :   A=326-356
     *                        ... 
     *                      }
     *          }
     * }
     * @throws IOException
     * @throws XPathExpressionException 
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @javax.ws.rs.Path("/ProtInfoByUniProtID")
    public Map<String, Map<String, Map<String, String>>> uniProtFetcherByID(@QueryParam("id") String id) throws IOException, XPathExpressionException {
        
        // check whether gene symbol or UniProtID is given
        if (id != null) {
            
            // fill gene_map
            return mapFromUniProtID(id);
            
        } else {
            // no valid input is given
            throw new IllegalArgumentException("Usage: localhost:8080/toUniProt/ProtInfo?genesymbol=BRCA1,... OR localhost:8080/toUniProt/ProtInfo?id=P38398,...");
        }

    }
    
    /**
     * creates gene map from genesymbols
     * @param genesymbols: String of comma-separated genesymbols
     * @return: Map containing gene information
     * @throws IOException
     * @throws XPathExpressionException 
     */
    private Map<String, Map<String, Map<String, String>>> mapFromGeneSymbol(String genesymbols) throws IOException, XPathExpressionException {
        
        // outer HashMap: collecting all information from UniProt for all input genes
        Map<String, Map<String, Map<String, String>>> gene_map = new HashMap<>();

        // gene symbols are given as input
        String[] genes = genesymbols.split(",");

        // loop through genes to extract UniProt information
        for (String gene : genes) {

            // read in content from cache.csv into Map cache
            //readFile();
            if (cache.containsKey(gene)) {
                gene_map.put(gene, cache.get(gene));
            } else {

                // format describes information type given as input
                String format = "gene_name";

                // first inner HashMap: collecting all information from UniProt for all input genes
                Map<String, Map<String, String>> single_gene_map = new UniProtConnector().getSingleGeneMap(format, gene);

                // put relevant information into cache
                cache.put(gene, single_gene_map);

                // add to outer HashMap
                gene_map.put(gene, single_gene_map);

            }

        }
        return gene_map;
    }
    
    /**
     * creates gene map from UniProtIDs
     * @param ids: String of comma-separated UniProtIDs
     * @return: Map containing gene information
     * @throws IOException
     * @throws XPathExpressionException 
     */
    private Map<String, Map<String, Map<String, String>>> mapFromUniProtID(String ids) throws IOException, XPathExpressionException {
        
        // outer HashMap: collecting all information from UniProt for all input genes
        Map<String, Map<String, Map<String, String>>> gene_map = new HashMap<>();

        // gene symbols are given as input
        String[] uniprot_ids = ids.split(",");

        // loop through genes to extract UniProt information
        for (String uniprot_id : uniprot_ids) {

            // read in content from cache.csv into Map cache
            // readFile();
            if (cache.containsKey(uniprot_id)) {
                gene_map.put(uniprot_id, cache.get(uniprot_id));
            } else {

                // format describes information type given as input
                String format = "UniProtID";

                // first inner HashMap: collecting all information from UniProt for all input genes
                Map<String, Map<String, String>> single_gene_map = new UniProtConnector().getSingleGeneMap(format, uniprot_id);

                // put relevant information into cache
                cache.put(uniprot_id, single_gene_map);

                // add to outer HashMap
                gene_map.put(uniprot_id, single_gene_map);

            }

        }
        return gene_map;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @javax.ws.rs.Path("/isoforms")
    public List<Isoform> getIsoforms(@QueryParam(value = "uniprotID") String uniprotID) throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        Document doc = helper.getDocument(uniprotID);
        List<Isoform> isoforms = new LinkedList<>();
        isoforms.add(helper.getCanonicalSequence(doc));
        isoforms.addAll(helper.getModifiedSequences(doc));
        // logger.trace("Got {} isoforms for {}", isoforms.size(), uniprotID);
        return isoforms;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/isoforms/alignmentPos")
    public List<AlignedSequence> getAlignmentPos(@QueryParam(value = "uniprotID") String uniprotID) throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        List<Isoform> isoforms = getIsoforms(uniprotID);
        ArrayList<AlignedSequence> sequences = new ArrayList<>();
        // Init all sequence objects with the sequence as single feature
        for (Isoform isoform : isoforms) {
            sequences.add(new AlignedSequence(isoform.getSequence(), isoform));
        }
        for (Isoform isoform : isoforms) {
            if (isoform.getModifications() == null) {
                continue;
            }
            for (Modification m : isoform.getModifications()) {
                for (AlignedSequence as : sequences) {
                    // logger.trace("Applying modification '{}' to sequence {}", m, as.getId());
                    as.applyModification(m, isoform, sequences);  // the current modification and the parent isoform
                }
            // break;
            }
        }
        return sequences;
    }

    @GET
    @Produces("image/svg+xml")
    @Path("/isoforms/svg")
    public String getSVGWithSequence(
            @QueryParam(value = "uniprotID") String uniprotID,
            @QueryParam(value = "sequence") String sequence,
            @QueryParam("color") String color)
            throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {

        int width = 1000;
        List<AlignedSequence> alignment = getAlignmentPos(uniprotID);

        StringBuilder svg = new StringBuilder();
        svg = helper.addSVGStart(svg, width, alignment.size());
        helper.addDBD(svg, sequence, alignment, width, validateColor("#" + color));
        svg = helper.addAlignmentsToSVG(svg, alignment, width);
        svg = helper.addSVGEnd(svg);
        return svg.toString();
    }

    private String validateColor(String color) {
        if (color == null) {
            color = "#AAAAAA";
        }
        Pattern pattern = Pattern.compile("^#[0-9A-Fa-f]{6}$");
        Matcher matcher = pattern.matcher(color);
        if (!matcher.find()) {
            color = "#AAAAAA";
        }
        return color;
    }

    @GET
    @Produces("image/svg+xml")
    @Path("/isoforms/svg_2")
    public String getSVG(@QueryParam(value = "uniprotID") String uniprotID) throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        int width = 1000;
        List<AlignedSequence> alignment = getAlignmentPos(uniprotID);

        // System.out.println("aa size " + aaSize);
        StringBuilder svg = new StringBuilder();
        svg = helper.addSVGStart(svg, width, alignment.size());
        svg = helper.addAlignmentsToSVG(svg, alignment, width);
        svg = helper.addSVGEnd(svg);
        return svg.toString();

    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/best/")
    public String selectBest(@QueryParam(value = "uniprotIDs") String uniprotIDs) throws IOException, SAXException, ParserConfigurationException, XPathExpressionException {

        List<UniProtQuality> items = new LinkedList<>();
        XPathFactory xPathfactory = XPathFactory.newInstance();

        for (String id : uniprotIDs.split(",")) {
            UniProtQuality prot = new UniProtQuality();
            prot.setId(id);

            Document doc = helper.getDocument(id);

            XPath xpath = xPathfactory.newXPath();
            XPathExpression expr = xpath.compile("/RDF/Description/reviewed");
            Node reviewedNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
            if (reviewedNode == null || reviewedNode.getTextContent().equals("false")) {
                prot.setReviewed(false);
            } else {
                prot.setReviewed(true);
            }

            xpath = xPathfactory.newXPath();
            expr = xpath.compile("/RDF/Description/obsolete");
            Node obsoleteNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
            if (obsoleteNode == null || reviewedNode.getTextContent().equals("false")) {
                prot.setObsolete(false);
            } else {
                prot.setObsolete(true);
            }

            xpath = xPathfactory.newXPath();
            expr = xpath.compile("/RDF/Description/existence");
            Node evidenceNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
            if (evidenceNode != null) {
                // System.out.println("node " + evidenceNode);
                // System.out.println("atts " + evidenceNode.getAttributes());
                // System.out.println("res " + evidenceNode.getAttributes().getNamedItem("rdf:resource"));
                prot.setLevel(evidenceNode.getAttributes().getNamedItem("rdf:resource").getNodeValue());
            }
            items.add(prot);
        }
        items.sort(new UniprotQualityComparator());
        System.out.println("sorted list " + items);
        return items.get(0).getId();
    }
}