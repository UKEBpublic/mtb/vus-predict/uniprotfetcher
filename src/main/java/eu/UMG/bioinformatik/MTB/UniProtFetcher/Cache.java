package eu.UMG.bioinformatik.MTB.UniProtFetcher;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;

/**
 * stores request results in a file as cache
 * @author vas
 */
@ApplicationScoped
public class Cache extends HashMap<String, Map<String, Map<String, String>>> {
    
    public final String filename = "src/main/resources/cache.csv";
    
    /**
     * Constructor: fills dictionary with cached results
     */
    public Cache() {
        super();
        FileReader fileReader = null;
        
        try {
            // object of fileReader class with csv file as parameter
            fileReader = new FileReader(filename);
            // create BufferedReader object passing file reader as input
            BufferedReader csvReader = new BufferedReader(fileReader);
            // traverse row-wise through csv file and store IDs in Map
            String row;
            while ((row = csvReader.readLine()) != null) {
                String[] row_data = row.split(",");
                String gene_symbol = row_data[0];
                String uniprot_id = row_data[1];
                // Map to store collected information in the right way
                Map<String, String> id_info = new HashMap<>();
                id_info.put("UniProtID", uniprot_id);
                // Map to put into cache with genesymbol as key
                Map<String, Map<String, String>> gene_info = new HashMap<>();
                gene_info.put("gene_info", id_info);
                // put into cache
                this.put(gene_symbol, gene_info);
            }
            csvReader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Cache.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Cache.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileReader.close();
            } catch (IOException ex) {
                Logger.getLogger(Cache.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public boolean containsKey(Object key) {
        return super.containsKey(key);

    }

    @Override
    public Map<String, Map<String, String>> put(String key, Map<String, Map<String, String>> value) {
        writeToFile();
        return super.put(key, value);
    }

    /**
     * writes content of cache to cache.csv
     *
     * @throws IOException
     */
    private void writeToFile() {

        try {
            // write to cache.csv
            FileWriter fileWriter = new FileWriter(filename);
            BufferedWriter csvWriter = new BufferedWriter(fileWriter);
            for (String key : this.keySet()) {
                // extract relevant information from UniProt
                String uniprot_id = this.get(key).get("gene_info").get("UniProtID");
                // write to file cache.csv in the following form: genesymbol,UniProtID,EnsemblGeneID,EnsemblTranscriptID,EnsemblProteinID
                String write_to_file = key + "," + uniprot_id;
                csvWriter.write(write_to_file);
                csvWriter.newLine();
            }

            // close writer
            csvWriter.close();
        } catch (IOException ex) {
            Logger.getLogger(Cache.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
}
