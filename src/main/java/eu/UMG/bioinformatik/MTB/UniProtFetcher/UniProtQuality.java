package eu.UMG.bioinformatik.MTB.UniProtFetcher;

class UniProtQuality {

    private String id;
    private boolean obsolete = false;
    private boolean reviewed = false;
    private int level = 0;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isObsolete() {
        return obsolete;
    }

    public void setObsolete(boolean obsolete) {
        this.obsolete = obsolete;
    }

    public boolean isReviewed() {
        return reviewed;
    }

    public void setReviewed(boolean reviewed) {
        this.reviewed = reviewed;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setLevel(String level) {
        if ("http://purl.uniprot.org/core/Evidence_at_Protein_Level_Existence".equals(level)) {
            this.level = 1;
        } else if ("http://purl.uniprot.org/core/Evidence_at_Transcript_Level_Existence".equals(level)) {
            this.level = 2;
        } else if ("http://purl.uniprot.org/core/Inferred_from_Homology_Existence".equals(level)) {
            this.level = 3;
        } else if ("http://purl.uniprot.org/core/Predicted_Existence".equals(level)) {
            this.level = 4;
        } else {
            System.out.println("level not found " + level);
        }
    }

    public String toString() {
        return String.format("%s {%b %d}", id, reviewed, level);
    }
    //1. Experimental evidence at protein level   <existence rdf:resource="http://purl.uniprot.org/core/Evidence_at_Protein_Level_Existence"/>
//2. Experimental evidence at transcript level    <existence rdf:resource="http://purl.uniprot.org/core/Evidence_at_Transcript_Level_Existence"/>
//3. Protein inferred from homology  http://purl.uniprot.org/core/Inferred_from_Homology_Existence
//4. Protein predicted http://purl.uniprot.org/core/Predicted_Existence
//5. Protein uncertain
}