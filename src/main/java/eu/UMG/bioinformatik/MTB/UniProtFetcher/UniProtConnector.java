package eu.UMG.bioinformatik.MTB.UniProtFetcher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * provide connection to UniProt to retrieve information
 * @author vas
 */
public class UniProtConnector {
    
    /**
     * create request_param map for a single request_param
     * @param format: one of "gene_symbol" or "UniProtID" for information processing
     * @param request_param: gene or UniProtID
     * @return Map for a single request_param containing request_param and PDB information
     * @throws IOException
     * @throws XPathExpressionException 
     */
    public Map<String, Map<String, String>> getSingleGeneMap(String format, String request_param) throws IOException, XPathExpressionException {

        // create Document for access in whole function
        Document doc;

        // inner HashMap: collecting all information from UniProt for all input genes
        Map<String, Map<String, String>> single_gene_map = new HashMap<>();
        
        // inner HashMap to store gene infos such as UniProtID, Ensembl Protein ID and Ensembl Transcript ID
        Map<String, String> gene_info;

        Map<String, String> gene_normalizer = new HashMap<>();
        
        // create second inner HashMap for given gene name and UniProtID separately
        if (format.equals("gene_name")) {
            // gene name was given as request_param
            
            // path to extract UniProt information
            String path = "https://www.ebi.ac.uk/proteins/api/proteins?offset=0&size=100&reviewed=true&exact_gene=" + request_param + "&organism=human";
            
            // retrieve Document of UniProt output
            doc = fetchUniprotOutputAsXML(path);
        
            // inner HashMap: store gene information (gene name, UniprotID, ..)
            gene_info = getUniprotIDs(doc);

            gene_normalizer = getGeneName(doc);
            
        }
        else {
            // UniProtID was given as request_param           
            // path to extract UniProt information
            String path = "https://www.ebi.ac.uk/proteins/api/proteins?offset=0&size=100&accession=" + request_param + "&reviewed=true&organism=human";
            
            // retrieve Document of UniProt output
            doc = fetchUniprotOutputAsXML(path);
          
            // inner HashMap: store gene name
            gene_info = getGeneName(doc);
            
        }
        
        // add to first inner HashMap
        single_gene_map.put("gene_info", gene_info);

        if (gene_normalizer != null){
            single_gene_map.put("gene_normalizer", gene_normalizer);
        }
        
        // inner HashMap: store PDB information (PDB-ID and chainID)
        Map<String, String> PDB_info = getPDBInformation(doc);
        // add to first inner HashMap
        single_gene_map.put("PDB_info", PDB_info);
        
        return single_gene_map;
        
    }
    
    
    /**
     * requests UniProt with a given path
     * @param path: path to request UniProt API
     * @return XML Document of UniProt output
     * @throws MalformedURLException
     * @throws IOException 
     */
    private Document fetchUniprotOutputAsXML(String path) throws MalformedURLException, IOException {

        // request sample code from: https://www.ebi.ac.uk/proteins/api/doc/index.html#!/proteins/search
        // create URL with given path
        URL url = new URL(path);

        URLConnection connection = url.openConnection();
        HttpURLConnection http_connection = (HttpURLConnection) connection;

        http_connection.setRequestProperty("Accept", "application/xml");

        InputStream response = connection.getInputStream();
        int responseCode = http_connection.getResponseCode();

        if (responseCode != 200) {
            throw new IllegalArgumentException();
        }

        // create XML output string for query
        String output;
        Reader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(response, "UTF-8"));
            StringBuilder builder = new StringBuilder();
            char[] buffer = new char[8192];
            int read;
            while ((read = reader.read(buffer, 0, buffer.length)) > 0) {
                builder.append(buffer, 0, read);
            }
            output = builder.toString();
        } finally {
            if (reader != null) try {
                reader.close();
            } catch (IOException logOrIgnore) {
                logOrIgnore.printStackTrace();
            }
        }
        
        // create document from XML input string
        Document doc = convertStringToDocument(output);
        
        return doc;
    }
    
    /**
     * conversion of UniProt String output to XML document
     * @param output: String output of http request of UniProt
     * @return XML document of String output
     */
    private Document convertStringToDocument(String output) {

        // convert XML string to document for further use
        // Build DOM
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        // factory.setNamespaceAware(true);
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(output)));
            return doc;
        } catch (IOException | ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * creates new XPath instance
     * @return new XPath instance
     */
    private XPath createXPath() {

        // create XPath
        XPathFactory xpathfactory = XPathFactory.newInstance();
        XPath xpath = xpathfactory.newXPath();
        return xpath;
    }
    
    /**
     * extract UniProtIDs from UniProt output XML document
     * @param doc: XML document of UniProt output
     * @return Map with UniProtID of requested protein
     * example for P53:
     * {UniProtID   :   P04637}
     * @throws XPathExpressionException 
     */
    private Map<String, String> getUniprotIDs(Document doc) throws XPathExpressionException {

        // create HashMap to store information on requested protein
        Map<String, String> gene_info = new HashMap<>();

        // create XPath
        XPath xpath = createXPath();

        // extract related UniprotIDs
        // UniprotIDs are located within "//entry/accession"
        XPathExpression expr = xpath.compile("//entry[1]/accession");
        Object result = expr.evaluate(doc, XPathConstants.NODESET);
        NodeList nodes = (NodeList) result;
        
        // item at first position contains best UniProtID
        String UniProt_ID = nodes.item(0).getTextContent();
        
        // store in HashMap to return
        gene_info.put("UniProtID", UniProt_ID);
        
        return gene_info;
    }
    
    /**
     * extract gene name from UniProt output XML document
     * @param doc: XML document of UniProt output
     * @return Map containing the gene name of the requested protein
     * example for P04637:
     * {gene_name   :   P53}
     * @throws XPathExpressionException 
     */
    // extract gene name from XML document
    private Map<String, String> getGeneName(Document doc) throws XPathExpressionException {
        
        // create HashMap to store information on requested protein
        Map<String, String> gene_info = new HashMap<>();

        // create XPath
        XPath xpath = createXPath();

        // extract related gene name
        // UniprotIDs are located within "//entry[1]/gene/name[@type='primary']"
        XPathExpression expr = xpath.compile("//entry[1]/gene/name[@type='primary']");
        Object result = expr.evaluate(doc, XPathConstants.NODESET);
        NodeList nodes = (NodeList) result;
        
        // item at first position contains gene name
        String gene_name = nodes.item(0).getTextContent();
        
        gene_info.put("gene_name", gene_name);
        
        return gene_info;
    }
    
    /**
     * extract PDB information from UniProt output XML document
     * @param doc: XML document of UniProt output
     * @return Map containing all available PDB IDs with its corresponding chain ID and positions
     * example for P53:
     * {1A1U   :   A/C=324-358
     *  1AIE   :   A=326-356
     *  ... }
     * @throws XPathExpressionException 
     */
    private Map<String, String> getPDBInformation(Document doc) throws XPathExpressionException {

        // create HashMap to store information on requested protein
        Map<String, String> PDB_info = new HashMap<>();

        // create XPath
        XPath xpath = createXPath();

        // extract PDB information
        // information relating to PDB are within "//entry/dbReference" nodes with "dbReference type" = "PDB"
        XPathExpression expr = xpath.compile("//entry[1]/dbReference[@type='PDB']");
        Object result = expr.evaluate(doc, XPathConstants.NODESET);
        NodeList nodes = (NodeList) result;
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            // item(0) equals the PDB ID (after getting attributes)
            String PDB_ID = node.getAttributes().item(0).getTextContent();
            NodeList child_nodes = node.getChildNodes();
            for (int k = 0; k < child_nodes.getLength(); k++) {
                Node child_node = child_nodes.item(k);
                if (child_node.getAttributes().item(0).getTextContent().contains("chains")) {
                    String chain_info = child_node.getAttributes().item(1).getTextContent();
                    PDB_info.put(PDB_ID, chain_info);
                }
            }
        }
        
        // return HashMap
        return PDB_info;
    }
    
}
